# Mistakes: Forgot to add arguments when calling the command

from sys import argv

script, filename = argv

print "We're going to erase %r." % filename
print "If you don't want that, hit CTRL-C (^C)."
print "If you do want that, hit RETURN."

raw_input("?")

print "Opening the file..."
# Passing 'w' to write to the file (Default is read)
target = open(filename, 'w')

print "Truncating the file. Goodbye!"
# Redundant because opening a file in 'w' mode will overwrite the file
target.truncate()

print "Now I'm going to ask you for three lines."

line1 = raw_input("line1: ")
line2 = raw_input("line2: ")
line3 = raw_input("line3: ")

print "I'm going to write these to the file."

#Compressed the writes to file
target.write(line1 + "\n" + line2 + "\n" + line3 + "\n")

print "And finally, we close it."
target.close()

print "\nNow to double check our work..."
new_target = open(filename, 'r')

print new_target.read()