# Importing module sys
from sys import argv

script, input_file = argv

# Print the whole file
def print_all(f):
    print f.read()

# Resets the file read. Requires the argument to be a file
def rewind(f):
    f.seek(0)

# Prints the integer and a file read out of a line
def print_a_line(line_count, f):
    print line_count, f.readline()

# Open the file
current_file = open(input_file)

print "First lets' print the whole file:\n"

# Calls the function. argument should be a file
print_all(current_file)

print "Now let's rewind, kind of like a tape."

# reset the file read. takes a argument of a file
rewind(current_file)

print "Let's print three liines:"

current_line = 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)