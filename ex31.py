#Study drill is to make it as crazy as possible

print "You enter a dark room with two doors. Do you go through door #1 or door #2?"

door = raw_input("> ")

if door == "1":
    print "There's a giant bear here eating a cheese cake. What do you do?"
    print "1. Take the cake."
    print "2. Scream at the bear."
    
    bear = raw_input("> ")
    
    if bear == "1":
        print "The bear eats your face off. Good job!"
    elif bear == "2":
        print "The bear eats your legs off. Good job!"
    else:
        print "Well doing %s is probably better. Bear runs away." % bear

elif door == "2":
    print "You stare into the endless abyss at Cthulhu's retina."
    print "1. Blueberries."
    print "2. Yellow jacket clothespins."
    print "3. Understanding revolvers yelling melodies."
    
    insanity = raw_input("> ")
    
    if insanity == "1" or insanity == "2":
        print "Your body survives powered by a mind of jello. Good job!"
    else:
        print "The insanity rots your eyes into a pool of much. Good job!"

# This is a hidden door
elif door == "3":
    print "Your sixth sense guides you to a room with an floating orb..."
    print "1. Grab the orb."
    print "2. Walk past the orb."
    
    orb = raw_input("> ")
    
    if orb == "1":
        print "The orb glitters in your hands... but the orb quickly rusts away.",
        print "You fall to the ground in uncontrollable tears..."
    elif orb == "2":
        print "You somehow made it outside! The sun greats your supple chest."
    else:
        print "The mist got to you before you could make a logical decision."

else:
    print "You stumble around and fall on a knife and die. Good job!"