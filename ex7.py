# Printing a string
print "Mary had a little lamb."
# Printing a string with a formatted string of 'snow'
print "Its fleece was white as %s." % 'snow'
# Printing a string
print "And everywhere that Mary went."
# Printing ten periods
print "." * 10 #Printing ',' ten times

# Assigning the string 'C' to the variable end1
end1 = "C"
# Assigning the string 'h' to the variable end2
end2 = "h"
# Assigning the string 'e' to the variable end3
end3 = "e"
# Assigning the string 'e' to the variable end4
end4 = "e"
# Assigning the string 's' to the variable end5
end5 = "s"
# Assigning the string 'e' to the variable end6
end6 = "e"
# Assigning the string 'B' to the variable end7
end7 = "B"
# Assigning the string 'u' to the variable end8
end8 = "u"
# Assigning the string 'r' to the variable end9
end9 = "r"
# Assigning the string 'g' to the variable end10
end10 = "g"
# Assigning the string 'e' to the variable end11
end11 = "e"
# Assigning the string 'r' to the variable end12
end12 = "r"

# The comma appends the next print statement
# Printing the variables end1, end2, end3, end4, end5, and end6
    # Also appending the next print statement with a comma
print end1 + end2 + end3 + end4 + end5 + end6,
# Printing the variable end7, end8, end9, end10, end11, and end12
print end7 + end8 + end9 + end10 + end11 + end12