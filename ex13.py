#Mistakes: Spacing issue on line 23. When adding a varible I don't
#   need to add extra space at the end of a string.

#importing module
from sys import argv

#unpacking argv
script, first, second, third = argv

print "Running...", script

# Printing all argv
print "Ahh... %r, %r, and %r. So many issues today." % (
    first, second, third)

#Grabbing user input with raw_input()
print "Is there anything else you want to tell me?",
more_info = raw_input()

#Changing into question
more_info_formatted = "Well %r is an issue. I am happy to say it is... " % more_info
happy_about = raw_input(more_info_formatted)

print "Let us just end with", happy_about