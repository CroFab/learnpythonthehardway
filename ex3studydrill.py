#Creating a tip calculator for exercise number 3

print("""\
Welcome to the Tip Calculator... 
Let us Begin...
""")

valid = 1
tipamount = 0
while valid:
    priceoftransaction = raw_input("Enter the price of the transaction: ")
    tippercent = raw_input("What percent do you want to tip (example: 20): ")
    
    #print("price: ", priceoftransaction, "tip%: ", tippercent)
    
    # verifying that values are non negative
    if int(priceoftransaction) > 0 and int(tippercent) > 0:
        priceoftransaction = float(priceoftransaction)
        tippercent = float(tippercent)
        tipamount = 0.0
        tipamount = priceoftransaction * (tippercent / 100.0)
        valid = 0
    else:
        print("Incorrect Values... \n")
      
print "\nYour tip should be...   $", tipamount


