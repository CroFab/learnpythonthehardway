# Here's some new strange stuff, remeber type it exactly

days = "Mon Tue Wed Thu Fri Sat Sun"
# \n is the new line character
months = "\nJan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

print "Here are the days: ", days
print "Here are the months: ", months

print """
There's something going on here.
with the three double-quotes.
We'll be able to type as mush as we like.
Even 4 lines if we want, or 5, or 6.
"""