#Learning about variables

# The variable cars has the integer 100 assigned to it
cars = 100

# The variable space_in_a_car has the float 4.0 assigned to it
space_in_a_car = 4.0

# The variable drivers has the integer 30 assigned to it
drivers = 30

# The variable passengers has the integer 90 assigned to it
passengers = 90

# The variable cars_not_driven has a negative integer assigned to it
cars_not_driven = cars - drivers

# The variable cars_driven is an integer
cars_driven = drivers

# The variable carpool_capacity is a float
carpool_capacity = cars_driven * space_in_a_car

# The variable average_passengers_per_car is a integer
average_passengers_per_car = passengers / cars_driven


print "There are", cars, "cars available."
print "There are only", drivers, "drivers available."
print "There will be", cars_not_driven, "empty cars today."
print "We can transport", carpool_capacity, "people today."
print "We have", passengers, "to carpool today."
print "We need to put about", average_passengers_per_car, "in each car."





