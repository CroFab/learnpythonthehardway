# Importing sys module
from sys import argv

# Unpacking argv
script, filename = argv

# Opening the variable named filename
txt = open(filename)

# Printing the filename
print "Here's your file %r:" % filename
# Reading the txt(file) and printing
print txt.read()

# Printing a string
print "Type the filename again:"
# Prompting for raw input
file_again = raw_input("> ")

# Opening the variable name txt_again
txt_again = open(file_again)

# Reading the txt_again(file) and printing
print txt_again.read()