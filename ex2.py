# This is a test of how comments work
# <-- pound sign, mesh, hash, or octothorpe

print("This is a test statement.") #This won't be printed

# Also using for disabling lines of code
# print("I can't print!!")

print("Finished")
