# This is a sting being saved to a variable named 'x' 
x = "There are %d types of people." % 10
# The string 'binary' is stored in the variable named 'binary'
binary = "binary"
# The sting 'don't' is stored in the variable named 'do_not'
do_not = "don't"
# The variables binary and do_not are being formatted to contain those
#   values in side of the current string and save them into the variable y
y = "Those who know %s and those who %s." % (binary, do_not)

# print the variable x
print x
# print the variable y
print y

# print the string that is formatted with the variable x
print "I said: %r." % x
# print the stirng that is formatted with the variable y
print "I also said: '%s'." % y

# The variable hilarious is equal to false (boolean)
hilarious = False
# The variable is being saved with a formatted reference that will be used later
joke_evaluation = "Isn't that joke so funny?! %r"

# The variable joke_evalution has the %r inside the string to allow the
# variable hilarious to be formatted correctly
print joke_evaluation % hilarious

# A string is being saved to the variable w
w = "This is the left side of..."
# A string is being saved to the variable y
e = "a string with a right side."

# Combining the variables of w and e which are both strings and printing
print w + e