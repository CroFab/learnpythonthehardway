name = 'Zed A. Shaw'
age = 35 # not a lie
height = 74 # inches
weight = 180 # lbs
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'

print "Let's talk about %s." % name
print "He's %d inches tall." % height
print "He's %d pounds heavy." % weight
print "Actually that's not too heavy."
print "He's got %r eyes and %s hair." % (eyes, hair)
print "His teeth are usually %s depending on the coffee." % teeth

# this line is a tricky, try to get it exactly right
print "If I add %r, %r, and %r I get %r." % (
    age, height, weight, age + height + weight)

#Converting to metric
inch_to_cm = 2.54
metric_height = height * inch_to_cm

pound_to_kg = 0.453592
metric_weight = weight * pound_to_kg

print "\nIf I convert %r inches to centimeters I get %r cm." % (
    height, metric_height)

print "If I convert %r pounds to kilograms I get %r kg." % (
    weight, metric_weight)
    