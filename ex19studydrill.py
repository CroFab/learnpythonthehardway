
def depositBankAccount(money):
    
    amountTotal = 0
    print "\nYou currently have $ %r in your account" % amountTotal
    
    try:
        if money < 0:
            print "Error! We don't accept negative numbers"
            return
        
        amountTotal = money + amountTotal
        print "You now have $ %r in your account" % amountTotal
        
    except TypeError:
        print "Type Error!"


bankCheck = 1000

# Normal calls
depositBankAccount(50)
depositBankAccount(100000)
depositBankAccount(0)
depositBankAccount(bankCheck)
depositBankAccount(50 * 50)
depositBankAccount(bankCheck * 50)

# Attempt to throw errors
depositBankAccount("$100")
depositBankAccount(-10)
depositBankAccount('Maybe')