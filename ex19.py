# Creating the function. Takes two arguments that should both be
#   integers. There is no precondition.
def cheese_and_crackers(cheese_count, boxes_of_crackers):
    print "You have %d chesses!" % cheese_count
    print "You have %d boxes of crackers!" % boxes_of_crackers
    print "Man that's enough for a party!"
    print "Get a blanket.\n"
    
print "We can just give the function numbers directly:"
# Calling the function with a set of integers
cheese_and_crackers(20, 30)

print "OR, we can use variables from our script:"
# Defining some variables
amount_of_cheese = 10
amount_of_crackers = 50

# Calling the function with a set of variables
cheese_and_crackers(amount_of_cheese, amount_of_crackers)

print "We can even do math insdie too:"
# Calling the function with a set of math equations
cheese_and_crackers(10 + 20, 5 + 6)

print "And we can combine the two, variables and math:"
# Calling the function with a set of variables and math
cheese_and_crackers(amount_of_cheese + 100, amount_of_crackers + 1000)